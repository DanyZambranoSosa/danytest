//
//  AppDelegate.swift
//  danymovietest
//


//Client ID: d499d36ab659201202aaa4fa20fab75cefa18e47f0f9e7b3fb54807af892ff38
//Client Secret: 508a468de35c0cec61a38f62d0b30f2666f2a207a53da12c79e902abb0f8d7ad
//https://api.trakt.tv



//Client ID: 90f0b806d95312206366a5a8a9c05f58092d1441251f7a38dda66d692776cfed
//Client Secret: 64e19d501b7eb3a1af251f388dc4f59d772c9534a3694eedc086a8b1889cfa44
//https://api-staging.trakt.tv





import UIKit
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let topRatedNavigationCtrl = storyboard.instantiateViewController(withIdentifier: "moviesNavigationCtrl") as! UINavigationController
        let topRatedVC = topRatedNavigationCtrl.topViewController as! MoviesViewController
        topRatedVC.endpoint = "top_rated"
        topRatedVC.title = "Mis Peliculas"
        topRatedNavigationCtrl.tabBarItem.image = #imageLiteral(resourceName: "Star")
        
        let tabBarCtrl = UITabBarController()
        tabBarCtrl.viewControllers = [ topRatedNavigationCtrl]
        
        window?.rootViewController = tabBarCtrl
        window?.makeKeyAndVisible()
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

